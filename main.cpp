#include <iostream>
#include <SFML/Graphics.hpp>
#include <string>
#include <sstream>
#include <list>


using namespace std;
namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}
// ����� ��������
class Item{
private:
    float x, y;
    sf::Sprite sprite;
    int width = 100;
    int height = 100;
    bool toRemove = false;
public:
    void setX(float o_x){
        x = o_x;
    }
    void setY(float o_y){
        y = o_y;
    }
    void setSprite(sf::Sprite o_sprite){
        sprite = o_sprite;
    }
    float getX(){
        return width;
    }
    float getWidth(){
        return height;
    }
    sf::Sprite getSprite(){
        sprite.setPosition(sf::Vector2f(x, y));
        return sprite;
    }
    void tick(float elapsed, int speed, float playerX, float playerY, int playerWidth, int playerHeight){
        y += speed * elapsed * 0.2;

        playerY = playerY + 20;
        playerWidth = playerWidth - 20;

        if ( (playerX <= x && playerX + playerWidth >= x ) || ( x <= playerX && x + width >= playerX ))
            if ( (playerY <= y && playerY + playerHeight >= y ) || ( y <= playerY && y + height >= playerY ))
                toRemove = true;
    }
    bool isToRemove(){
        return toRemove;
    }
};
// ��������
sf::Texture backgroundTexture;
sf::Sprite backgroundSprite;
sf::Texture playerTexture;
sf::Sprite playerSprite;
sf::Texture eat1;
// ���������� ������
int width = 1000;
int height = 563;
int playerWidth = 200;
int playerHeight = 258;
float x = width / 2 - playerWidth / 2;
float y = height - 60 - playerHeight;
// ����
 sf::RenderWindow window(sf::VideoMode(width, height), "My Little Game");
// �������������
void init(){
    // ���
    backgroundTexture.loadFromFile("background.png");
    backgroundSprite.setTexture(backgroundTexture);
    backgroundSprite.setPosition(sf::Vector2f(0, 0));
    // ����
    playerTexture.loadFromFile("player.png");
    playerSprite.setTexture(playerTexture);
    playerSprite.setOrigin(sf::Vector2f(0, 0));
    playerSprite.setPosition(sf::Vector2f(0, 0));
    // ���
    eat1.loadFromFile("item1.png");
}

int random(int min, int max) //range : [min, max)
{
   static bool first = true;
   if (first)
   {
      srand( time(NULL) ); //seeding for the first time only!
      first = false;
   }
   return min + rand() % (( max + 1 ) - min);
}
//-------------------------------------------------------------------------------------------

float shag = 500; list<Item> items; float latestTime = 0;
void generateItem(){
    Item item;
    item.setX(random(0, width - 100));
    item.setY(0);

    sf::Sprite sprite;
    sprite.setTexture(eat1);
    sprite.setOrigin(sf::Vector2f(0, 0));
    sprite.setPosition(sf::Vector2f(0, 0));

    item.setSprite(sprite);

    items.push_back(item);
}
void tick(float elapsed){
    // ��������
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || sf::Keyboard::isKeyPressed(sf::Keyboard::A))
        x = x - (shag * elapsed);
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) || sf::Keyboard::isKeyPressed(sf::Keyboard::D))
        x = x + (shag * elapsed);
    // ���������� �� ��������
    int count = 0;
    std::list<Item>::iterator it;
    for (it = items.begin(); it != items.end();){

        it->tick(elapsed, shag, x, y, playerWidth, playerHeight);
        if (it->isToRemove()){
            it = items.erase(it);
        } else {
            count++;
            ++it;
        }
    }

    if (count < 10 && latestTime <= 0){
        latestTime = 1000 / shag;
        generateItem();
    }
    if (latestTime >= 0) latestTime -= elapsed;
    // �������� ����
    shag += elapsed * 25;
    window.setTitle( patch::to_string(shag) );
    // ��������� �������
    playerSprite.setPosition(sf::Vector2f(x, y));
}

//-------------------------------------------------------------------------------------------
void render(){
    window.draw(backgroundSprite);
    std::list<Item>::iterator it;
    for (it = items.begin(); it != items.end(); ++it){
        window.draw(it->getSprite());
    }
    window.draw(playerSprite);
}
//-------------------------------------------------------------------------------------------
// ������
int main()
{
    init();
    sf::Clock clock;
    //-------------------------------------------------------------------------------------------
    // ������
    while (window.isOpen())
    {
        //---------------------------------------------------------------------------------------
        sf::Event event;
        while (window.pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed)
                window.close();
        }
        //---------------------------------------------------------------------------------------
        // clear the window with black color
        window.clear(sf::Color::White);
        //---------------------------------------------------------------------------------------
        // draw everything here...
        sf::Time elapsed = clock.restart();
        tick(elapsed.asSeconds());
        render();
        //window.setTitle(patch::to_string(1 / (elapsed.asSeconds())));
        //---------------------------------------------------------------------------------------
        // end the current frame
        window.display();
    }
    //-------------------------------------------------------------------------------------------
    return 0;
}
